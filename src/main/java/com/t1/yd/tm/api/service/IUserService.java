package com.t1.yd.tm.api.service;

import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.model.User;

public interface IUserService extends IUserRepository {

    User removeById(String id);

    User removeByLogin(String login);

    User removeByEmail(String email);

    User setPassword(String id, String password);

    User updateUser(String id, String firstName, String lastName, String middleName);

}
