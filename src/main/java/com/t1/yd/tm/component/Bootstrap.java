package com.t1.yd.tm.component;

import com.t1.yd.tm.api.repository.ICommandRepository;
import com.t1.yd.tm.api.repository.IProjectRepository;
import com.t1.yd.tm.api.repository.ITaskRepository;
import com.t1.yd.tm.api.repository.IUserRepository;
import com.t1.yd.tm.api.service.*;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.command.project.*;
import com.t1.yd.tm.command.system.*;
import com.t1.yd.tm.command.task.*;
import com.t1.yd.tm.command.user.*;
import com.t1.yd.tm.enumerated.Role;
import com.t1.yd.tm.exception.system.ArgumentNotSupportedException;
import com.t1.yd.tm.exception.system.CommandNotSupportedException;
import com.t1.yd.tm.model.Project;
import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.repository.CommandRepository;
import com.t1.yd.tm.repository.ProjectRepository;
import com.t1.yd.tm.repository.TaskRepository;
import com.t1.yd.tm.repository.UserRepository;
import com.t1.yd.tm.service.*;
import com.t1.yd.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final IUserRepository userRepository = new UserRepository();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    private final ILoggerService loggerService = new LoggerService();

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserPasswordChangeCommand());
        registry(new UserRegistryCommand());
        registry(new UserShowProfileCommand());
        registry(new UserUpdateProfileCommand());
    }


    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    public void run(String[] args) {
        initDemoData();
        initLogger();

        processArguments(args);

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                System.err.println(e.getMessage());
                System.err.println("[FAIL]");
                loggerService.error(e);
            }
        }
    }

    private void initDemoData() {
        projectRepository.add(new Project("project1", "my first project"));
        projectRepository.add(new Project("project2", "my 2nd project"));
        projectRepository.add(new Project("project3", "my 3rd project"));
        projectRepository.add(new Project("project4", "my 4th project"));
        projectRepository.add(new Project("project5", "my 5th project"));

        taskRepository.add(new Task("task1", "my 1st task"));
        taskRepository.add(new Task("task2", "my 2nd task"));
        taskRepository.add(new Task("task3", "my 3rd task"));
        taskRepository.add(new Task("task4", "my 4th task"));

        userService.add(userService.create("admin", "admin", Role.ADMIN));
        userService.add(userService.create("user1", "user1", "user1@mail.ru"));
        userService.add(userService.create("user2", "user2", "user2@mail.ru"));

        projectTaskService.bindTaskToProject(taskRepository.findOneByIndex(0).getId(), projectRepository.findOneByIndex(0).getId());
        projectTaskService.bindTaskToProject(taskRepository.findOneByIndex(1).getId(), projectRepository.findOneByIndex(0).getId());
    }

    private void processArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        processArgument(args[0]);
    }

    private void processArgument(final String arg) {
        final AbstractCommand command = commandService.getCommandByArgument(arg);
        if (command == null) throw new ArgumentNotSupportedException();
        command.execute();
    }

    private void processCommand(final String commandName) {
        final AbstractCommand command = commandService.getCommandByName(commandName);
        if (command == null) throw new CommandNotSupportedException();
        command.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK MANAGER IS SHUTTING DOWN **")));
    }

    private void exit() {
        System.exit(0);
    }

}
