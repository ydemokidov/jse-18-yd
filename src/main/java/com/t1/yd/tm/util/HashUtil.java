package com.t1.yd.tm.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    String SECRET = "qwerty";
    Integer ITERATION = 1234;

    static String salt(final String value) {
        if (value == null || value.isEmpty()) return null;
        String result = value;
        for (int i = 0; i < ITERATION; i++) {
            result = md5(SECRET + result + SECRET);
        }
        return result;
    }

    static String md5(final String value) {
        if (value == null || value.isEmpty()) return null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuffer stringBuffer = new StringBuffer();
            for (byte b : array) {
                stringBuffer.append(Integer.toHexString((b & 0xFF) | 0x100).substring(1, 3));
            }
            return stringBuffer.toString();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        return null;
    }

}
