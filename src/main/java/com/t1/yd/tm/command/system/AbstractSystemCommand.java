package com.t1.yd.tm.command.system;

import com.t1.yd.tm.api.service.ICommandService;
import com.t1.yd.tm.command.AbstractCommand;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

}
