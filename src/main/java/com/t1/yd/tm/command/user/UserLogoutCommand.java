package com.t1.yd.tm.command.user;

public class UserLogoutCommand extends AbstractUserCommand {

    private final String name = "user_logout";
    private final String description = "User logout";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        getAuthService().logout();
        System.out.println("[USER LOGGED OUT]");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
