package com.t1.yd.tm.command.task;

import com.t1.yd.tm.model.Task;
import com.t1.yd.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task_list_by_project_id";
    public static final String DESCRIPTION = "Show list of project tasks";

    @Override
    public void execute() {
        System.out.println("[SHOW TASKS BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(projectId);

        int index = 1;
        for (Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
