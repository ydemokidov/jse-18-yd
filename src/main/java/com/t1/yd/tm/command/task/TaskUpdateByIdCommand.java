package com.t1.yd.tm.command.task;

import com.t1.yd.tm.util.TerminalUtil;

public final class TaskUpdateByIdCommand extends AbstractTaskCommand {

    public static final String NAME = "task_update_by_id";
    public static final String DESCRIPTION = "Update task by Id";

    @Override
    public void execute() {
        System.out.println("[UPDATE TASK BY ID]");

        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();

        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();

        System.out.println("ENTER DESCRIPTION:");
        final String desc = TerminalUtil.nextLine();

        getTaskService().updateById(id, name, desc);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
