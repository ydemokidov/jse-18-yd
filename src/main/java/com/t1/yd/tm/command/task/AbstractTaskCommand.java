package com.t1.yd.tm.command.task;

import com.t1.yd.tm.api.service.IProjectTaskService;
import com.t1.yd.tm.api.service.ITaskService;
import com.t1.yd.tm.command.AbstractCommand;
import com.t1.yd.tm.enumerated.Status;
import com.t1.yd.tm.model.Task;

public abstract class AbstractTaskCommand extends AbstractCommand {

    ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @Override
    public String getArgument() {
        return null;
    }

    protected void showTask(final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESC: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

}
