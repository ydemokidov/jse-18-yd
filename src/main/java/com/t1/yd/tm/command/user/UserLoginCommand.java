package com.t1.yd.tm.command.user;

import com.t1.yd.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    private final String name = "user_login";

    private final String description = "User login";

    @Override
    public void execute() {
        System.out.println("[USER LOGIN]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        getAuthService().login(login, password);
        System.out.println("[USER " + login + " LOGGED IN]");
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
