package com.t1.yd.tm.command.user;

import com.t1.yd.tm.model.User;
import com.t1.yd.tm.util.TerminalUtil;

public class UserRegistryCommand extends AbstractUserCommand {

    private final String name = "user_registry";

    private final String description = "Create new user";

    @Override
    public void execute() {
        System.out.println("[CREATE NEW USER]");
        System.out.println("[ENTER LOGIN:]");
        final String login = TerminalUtil.nextLine();
        System.out.println("[ENTER PASSWORD:]");
        final String password = TerminalUtil.nextLine();
        System.out.println("[ENTER EMAIL:]");
        final String email = TerminalUtil.nextLine();
        User user = getAuthService().registry(login, password, email);
        System.out.println("[USER CREATED]");
        showUser(user);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
