package com.t1.yd.tm.exception.field;

public class NameEmptyException extends AbstractFieldException {

    public NameEmptyException() {
        super("Error! Name is empty...");
    }
}
